FROM ubuntu:16.04
LABEL Description="This image install bitcoind from the official bitcoin launchpad"

RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list

RUN apt-get update && \
    apt-get install -y bitcoind && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN groupadd -r bitcoin && useradd -r -m -g bitcoin bitcoin
ENV BITCOIN_DATA /data
RUN mkdir $BITCOIN_DATA
COPY bitcoin.conf $BITCOIN_DATA/bitcoin.conf
RUN chown bitcoin:bitcoin -R $BITCOIN_DATA && \
    ln -s $BITCOIN_DATA /home/bitcoin/.bitcoin

USER bitcoin
VOLUME /data
EXPOSE 8332 8333
CMD ["/usr/bin/bitcoind", "-conf=/data/bitcoin.conf", "-datadir=/data", "-server", "-txindex", "-printtoconsole"]